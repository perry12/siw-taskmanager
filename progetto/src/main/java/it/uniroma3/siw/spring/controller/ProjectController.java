package it.uniroma3.siw.spring.controller;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.spring.controller.session.SessionData;
import it.uniroma3.siw.spring.controller.validation.ProjectValidator;
import it.uniroma3.siw.spring.model.Credentials;
import it.uniroma3.siw.spring.model.Project;
import it.uniroma3.siw.spring.model.Tag;
import it.uniroma3.siw.spring.model.Task;
import it.uniroma3.siw.spring.model.User;
import it.uniroma3.siw.spring.service.CredentialsService;
import it.uniroma3.siw.spring.service.ProjectService;
import it.uniroma3.siw.spring.service.TagService;
import it.uniroma3.siw.spring.service.UserService;

@Controller
public class ProjectController {
	
	@Autowired 
	ProjectService projectService;
	
	@Autowired
	UserService userService;
	
	@Autowired
    ProjectValidator projectValidator;
	
	@Autowired
	TagService tagService;
	
	@Autowired
	SessionData sessionData;
	
	@Autowired
	CredentialsService credentialsService;
	
	private Project currentProject;
	
	/*CU: Visualizza i miei progetti*/
	@RequestMapping(value = {"/projects"}, method = RequestMethod.GET)
	public String myOwnedProjects(Model model) {
		User loggedUser = sessionData.getLoggedUser();
		
		//prende i progetti dell'utente proprietario
		List<Project> projectsList = projectService.retrieveProjectsOwnedBy(loggedUser);
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("projectsList", projectsList);
		return "myOwnedProjects";
	}
	/*CU: Visualizza i miei progetti*/
	
	/* Visualizza i progetti di cui ho visibilità */
	@RequestMapping(value = {"/sharedWithMe"}, method = RequestMethod.GET)
	public String myVisibleProjects(Model model) {
		User loggedUser = sessionData.getLoggedUser();
		
		//prende i progetti di cui ho visibilità ed elimina i duplicati
		List<Project> projectsList1 = loggedUser.getVisibleProjects();
	    projectsList1 = new ArrayList<Project>(new LinkedHashSet<Project>(projectsList1));
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("sharedProjectsList", projectsList1);
		return "myVisibleProjects";
	}
	
	/* Visualizza la lista di tutti gli utenti per condividere il progetto */
	@RequestMapping(value = {"/projects/{projectId}/users"}, method = RequestMethod.GET)
	public String usersList(Model model, @PathVariable Long projectId) {
		Project project = projectService.getProject(projectId);
		User loggedUser = sessionData.getLoggedUser();
		
		//prende le credenziali di tutti gli utenti del sistema ed elimina il loggedUser
		List<Credentials> allCredentials = this.credentialsService.getAllCredentials();
		allCredentials.remove(sessionData.getLoggedCredentials());
		model.addAttribute("project", project);
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("credentialsList", allCredentials);
		return "allUsersForShare";
	}
		
	/* Condivide un progetto con un utente, salvandolo nella sua lista dei progetti visibili*/
	@RequestMapping(value = {"/projects/{projectId}/{userId}/share"}, method = RequestMethod.POST)
	public String shareProject(Model model, @PathVariable Long projectId, @PathVariable Long userId) {
		Project project = projectService.getProject(projectId);
		User loggedUser = sessionData.getLoggedUser();
		User user = userService.getUser(userId);
		if(!projectService.isShared(project, user)) {
			this.projectService.shareProjectWithUser(project, user);
		}
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("project", project);
		return "redirect:/projects/{projectId}/users";
	}
	
	/* Toglie la condivisione di un progetto con un utente, rimuovendolo dalla sua lista dei progetti visibili*/
	@RequestMapping(value = {"/projects/{projectId}/{userId}/unshared"}, method = RequestMethod.POST)
	public String unsharedProject(Model model, @PathVariable Long projectId, @PathVariable Long userId) {
		Project project = projectService.getProject(projectId);
		User loggedUser = sessionData.getLoggedUser();
		User user = userService.getUser(userId);
		if(projectService.isShared(project, user)) {
			this.projectService.unsharedProjectWithUser(project, user);
		}
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("project", project);
		return "redirect:/projects/{projectId}/users";
	}
	
	/*CU: visualizza un progetto specifico*/
	@RequestMapping(value = {"/projects/{projectId}"}, method = RequestMethod.GET)
	public String project(Model model, @PathVariable Long projectId) {
		
		Project project = projectService.getProject(projectId);
		
		if(project == null)
			return "redirect:/projects";
		
		//prende i task, i tag e i membri del progetto
		List<User> members = userService.getMembers(project);
		List<Task> tasks = project.getTasks();
		List<Tag> tags = project.getTags();
		
		//controlla che il proprietario del progetto non sia il loggedUser e non sia nella lista dei membri del progetto  
		if(!project.getOwner().equals(sessionData.getLoggedUser()) && !members.contains(sessionData.getLoggedUser()))
			return "redirect:/projects";
		
		model.addAttribute("loggedUser", sessionData.getLoggedUser());
		model.addAttribute("project", project);
		model.addAttribute("members", members);
		model.addAttribute("tasks", tasks);
		model.addAttribute("tags", tags);

		return "project";		
	}
	/*CU: visualizza un progetto specifico*/

	/*CU: crea un nuovo progetto*/
	@RequestMapping(value = {"/projects/add"}, method = RequestMethod.GET)
	public String createProjectForm(Model model) {
		
		User loggedUser = sessionData.getLoggedUser();
		
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("projectForm", new  Project());
		
		return "addProject";
	}
	
	@RequestMapping(value = {"/projects/add"}, method = RequestMethod.POST)
	public String createProject(	@Valid @ModelAttribute("projectForm") Project project,
									BindingResult projectBindingResult,
									Model model ) {
		
		User loggedUser = sessionData.getLoggedUser();
		
		//convalida i dati del progetto
		projectValidator.validate(project, projectBindingResult);
		
		if(!projectBindingResult.hasErrors()) {
			project.setOwner(loggedUser);
			this.projectService.saveProject(project);
			return "redirect:/projects/" + project.getId();
		}
		
		model.addAttribute("loggedUser", loggedUser);
		
		return "addProject";
	}
	/*CU: crea un nuovo progetto*/
	
	/*CU: aggiungi un tag ad un progetto */
	@RequestMapping(value = {"/projects/{projectId}/tag/add"}, method = RequestMethod.GET)
	public String createProjectTagForm(Model model,  @PathVariable Long projectId) {
		
		User loggedUser = sessionData.getLoggedUser();
		Project project = projectService.getProject(projectId);
		
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("project", project);
		model.addAttribute("tagForm", new  Tag());
		
		return "addTag";
	}
	
	@RequestMapping(value = {"/projects/{projectId}/tag/add"}, method = RequestMethod.POST)
	public String createProjectTag( @Valid @ModelAttribute("tagForm") Tag tag,
									Model model,
									@PathVariable Long projectId) {
		
		Project project = projectService.getProject(projectId);
		User loggedUser = sessionData.getLoggedUser();
		
		//controlla se il loggedUser è il proprietario del progetto
		if(loggedUser.equals(project.getOwner())) {
			project.addTag(tag);
			projectService.saveProject(project);
		}
		
		model.addAttribute("loggedUser", loggedUser);
		
		return "redirect:/projects/{projectId}";
	}
	/*CU: aggiungi un tag ad un progetto */

	
	/* CU: Aggiornare i dati di un mio progetto */
	@RequestMapping(value = {"/projects/{projectId}/edit"}, method = RequestMethod.GET)
	public String showUpdateProjectForm(Model model, @PathVariable Long projectId) {
		
		this.currentProject = this.projectService.getProject(projectId);
		model.addAttribute("projectForm", currentProject);
		
		return "updateProject";
	}
	
	@RequestMapping(value = {"/projects/{projectId}/edit"}, method = RequestMethod.POST)
	public String confirmUpdateProject(	@Valid @ModelAttribute("projectForm") Project project,
										BindingResult projectBindingResult,
										Model model ) {
		
		this.currentProject.setName(project.getName());
		this.currentProject.setDescription(project.getDescription());
		
		projectValidator.validate(this.currentProject, projectBindingResult);
		if(!projectBindingResult.hasErrors()) {
			this.projectService.saveProject(this.currentProject);
			return "redirect:/projects/" + this.currentProject.getId();
		}
		return "updateProject";
	}
	/* CU: Aggiornare i dati di un mio progetto */

	/* CU: Cancellare un mio progetto */
	@RequestMapping( value = { "/projects/{projectId}/delete" }, method = RequestMethod.POST)
	public String deleteProject(@PathVariable Long projectId, Model model) {
		Project project = this.projectService.getProject(projectId);
		this.projectService.deleteProject(project);
		return "redirect:/projects";
	}
	/* CU: Cancellare un mio progetto */
}
