package it.uniroma3.siw.spring.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
public class Comment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "task_id")
	private Task relativeTask;

	@Column(nullable = false)
	private String text;
	
	@OneToOne
	@JoinColumn(name = "author_id")
	private User author;
	
	@Column(updatable = false)
	private LocalDateTime creationTimeStamp;
	
	@Column
	private LocalDateTime lastUpdateTimeStamp;

	public Comment() {
	}

	public Comment(Task relativeTask, String text, User author) {
		this.relativeTask = relativeTask;
		this.text = text;
		this.author = author;
	}
	
	@PrePersist
	public void onPersist() {
		this.creationTimeStamp = LocalDateTime.now();
		this.lastUpdateTimeStamp = LocalDateTime.now();
	}
	
	@PreUpdate
	public void onUpdate() {
		this.lastUpdateTimeStamp = LocalDateTime.now();
	}

	public Task getRelativeTask() {
		return relativeTask;
	}

	public void setRelativeTask(Task relativeTask) {
		this.relativeTask = relativeTask;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public LocalDateTime getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(LocalDateTime creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public LocalDateTime getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(LocalDateTime lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result + ((relativeTask == null) ? 0 : relativeTask.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (relativeTask == null) {
			if (other.relativeTask != null)
				return false;
		} else if (!relativeTask.equals(other.relativeTask))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [relativeTask=" + relativeTask + ", text=" + text + ", author=" + author + "]";
	}
	
}
