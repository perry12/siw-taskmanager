package it.uniroma3.siw.spring.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.spring.controller.session.SessionData;
import it.uniroma3.siw.spring.controller.validation.CredentialsValidator;
import it.uniroma3.siw.spring.controller.validation.NewCredentialsValidator;
import it.uniroma3.siw.spring.controller.validation.UserValidator;
import it.uniroma3.siw.spring.model.Credentials;
import it.uniroma3.siw.spring.model.User;
import it.uniroma3.siw.spring.service.CredentialsService;
import it.uniroma3.siw.spring.service.UserService;

@Controller
public class AuthenticationController {
	
	@Autowired
	CredentialsService credentialsService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserValidator userValidator;
	
	@Autowired
	NewCredentialsValidator newCredentialsValidator;
	
	@Autowired
	CredentialsValidator credentialsValidator;

	@Autowired
    SessionData sessionData;
	
	@RequestMapping(value = {"/users/register"}, method = RequestMethod.GET)
	public String showRegisterForm(Model model) {
		model.addAttribute("userForm", new User());
		model.addAttribute("credentialsForm", new Credentials());
		return "registerUser";
	}
	
	@RequestMapping(value = {"/users/register"}, method = RequestMethod.POST)
	public String registerUser( @Valid @ModelAttribute("userForm") User user,
								BindingResult userBindingResult,
								@Valid @ModelAttribute("credentialsForm") Credentials credentials,
								BindingResult credentialsBindingResult,
								Model model ) {
		
		this.userValidator.validate(user, userBindingResult);
		this.newCredentialsValidator.validate(credentials, credentialsBindingResult);
		
		if(!userBindingResult.hasErrors() && !credentialsBindingResult.hasErrors()) {
			credentials.setUser(user);
			credentialsService.saveCredentials(credentials);
			return "registrationSuccessful";
		}
		return "registerUser";
	}

	/* CU: Aggiornare il mio profilo */
	@RequestMapping(value = {"/users/me/update"}, method = RequestMethod.GET)
	public String showUpdateUserForm(Model model) {
		
		User loggedUser = sessionData.getLoggedUser();
		Credentials loggedUserCredentials = sessionData.getLoggedCredentials();

		model.addAttribute("userForm", loggedUser);
		model.addAttribute("credentialsForm", loggedUserCredentials);

		return "updateUser";
	}
	
	@RequestMapping(value = {"/users/me/update"}, method = RequestMethod.POST)
	public String updateUser( @Valid @ModelAttribute("userForm") User user,
							  BindingResult userBindingResult,
							  @Valid @ModelAttribute("credentialsForm") Credentials credentials,
							  BindingResult credentialsBindingResult,
							  Model model) {
		
		
		Credentials loggedUserCredentials = sessionData.getLoggedCredentials();
		User loggedUser = loggedUserCredentials.getUser();
		
		/* aggiorna e convalida username */
		loggedUserCredentials.setUsername(credentials.getUsername());
		this.credentialsValidator.validateUsername(credentialsBindingResult, loggedUserCredentials.getUsername());
		
		/* aggiorna e convalida password */
		String campoPassword = credentials.getPassword().trim();
		if(!campoPassword.isEmpty()) {
			this.credentialsValidator.validatePassword(credentialsBindingResult, campoPassword);
			loggedUserCredentials.setPassword(this.credentialsService.getPasswordEncoder().encode(campoPassword));
		}
		
		/* aggiorna e convalida dati di user */
		loggedUser.setFirstName(user.getFirstName());
		loggedUser.setLastName(user.getLastName());
		this.userValidator.validate(loggedUser, userBindingResult);
		
		if(!userBindingResult.hasErrors() && !credentialsBindingResult.hasErrors()) {
			this.credentialsService.updateCredentials(loggedUserCredentials);
			return "redirect:/users/me";
		}
		return "updateUser";
	}
	/* CU: Aggiornare il mio profilo */
}
